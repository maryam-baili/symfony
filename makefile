#!/usr/bin/env bash
ENV_BUILD:=$(shell test -s .env || cp .env.dist .env)

include .env

SYMFONY_TEST_IMAGE=init_container_${SYMFONY_VERSION}_${PHP_VERSION}

help: ## Show the commands description
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

build: ## Build and initialize the project
ifeq (,$(wildcard ./app/composer.json))
		@echo "Project empty! Initializing Symfony ${SYMFONY_VERSION} over php ${PHP_VERSION}!"
		@docker rm ${SYMFONY_TEST_IMAGE}_container -f || true
		@docker build --pull --cache-from $(SYMFONY_RELEASE_IMAGE) --tag ${SYMFONY_TEST_IMAGE} . \
		 --build-arg SYMFONY_VERSION=${SYMFONY_VERSION} \
		 --build-arg PHP_VERSION=${PHP_VERSION} \
		 -f ./docker/Dockerfile #--no-cache
		@docker run --detach --name=${SYMFONY_TEST_IMAGE}_container ${SYMFONY_TEST_IMAGE}
		@docker cp ${SYMFONY_TEST_IMAGE}_container:/var/www/html/app/ ./
		@docker rm ${SYMFONY_TEST_IMAGE}_container -f
		@cp app/.env app/env.dist
endif
	@docker-compose up -d --build --force-recreate
	@docker-compose exec -T php sh -c 'ls -ll && composer install'

run: ## Run an already built project
	@docker-compose up -d

stop: ## Stop an already running project
	@docker-compose stop

destroy: ## Destroy an already built project
	@docker-compose down
	@docker-compose rm -f
	
build-sonar: ## build sonarqube server image
	@docker build -t sonarqube -f sonar-server-dockerfile --rm .

run-sonar: ## run sonarqube image.
	@docker run -d --name=sonarqube -p 7000:9000 sonarqube

